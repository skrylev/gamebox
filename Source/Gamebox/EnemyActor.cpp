// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Weapon/Weapon.h"

AEnemyActor::AEnemyActor()
{
	PrimaryActorTick.bCanEverTick = true;

    Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
    Body->SetupAttachment(RootComponent);
    Body->SetEnableGravity(true);
    Body->SetSimulatePhysics(true);
    
    Head = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head"));
    Head->SetupAttachment(Body);
	   
    Muzzle = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Muzzle"));
    Muzzle->SetupAttachment(Head);
	   
    MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
    MuzzleLocation->SetupAttachment(Muzzle);
}

void AEnemyActor::PickUpWeapon(AWeapon* Weapon)
{
}

void AEnemyActor::BeginPlay()
{
	Super::BeginPlay();
	auto Weapon = Cast<AWeapon>(GetWorld()->SpawnActor(DefaultWeapon));
	Weapon->AttachToComponent(MuzzleLocation, FAttachmentTransformRules::KeepRelativeTransform);
	SetCurrentWeapon(Weapon);
}
